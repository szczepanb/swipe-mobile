window.Inpl = window.Inpl || {};
window.Inpl.Swipe = function (options) {
	// Private variables
	var vars = {
		handlers:{
			columns: null
		},
		blockActiveElement: null,
		countColumns: 0,
		slide: true,
		block: false,
		time: null,
		touch:{
			first: null,
			x: 0,
            y: 0
		},
        interval: null
	},
	// Options widget
	defaults = {
		// target element of swipe which event are set
		target: null,
		// Active slide
		active: 1,
		// Touch duration to change slide
		duration: 250,
		// On / Off carousel
		continuous: true,
        // Slideshow time in miliseconds to change slide. 0 - slidshow off
        auto: 0,
		// Required classes to work
		classes: {
				columns: ':scope > .swipe',
				column: ':scope > .swipe > div'
		},
		// Touch options
		touch: {
				// Tolerance in px from start swipe
				blockTolerance: 20,
                // Tolerance in px on Y move touch.
				blockYTolerance: 20,
				// Tolerance in % from left/right side to start swipe work.
				startTolerance: 100,
				// Tolerance to switch slide
				tolerance: 50
		},
		events:{
			onInitColumn: null
		}
    },
    private_function = {
		// On touch start event
		touchStart: function (e) {
			e.stopPropagation();
			if (!vars.slide) {
					return false;
			}
            private_function.offSlideshow();
			var touch = (e.changedTouches || e.touches || e.originalEvent.touches)[0];

			if (vars.touch.first !== null) {
					return false;
			}
			vars.touch.first = touch;
			vars.touch.x = touch.clientX;
			vars.touch.y = touch.clientY;
			vars.time = new Date();
		},
		// On touch move event
		touchMove: function (e) {
			e.stopPropagation();
			if (!vars.slide) {
				return false;
			}

			var touch = (e.changedTouches || e.touches || e.originalEvent.touches)[0];
            var movex = vars.touch.x - touch.clientX;
			var movey = vars.touch.y - touch.clientY;

			if (!private_function.isFirstTouchEvent(touch) || (Math.abs(movey) > defaults.touch.blockYTolerance && Math.abs(movex) < defaults.touch.blockTolerance)) {
				return false;
			}

			if(defaults.continuous){
				private_function.setFirstLastPosition(movex);
			}

			private_function.moveSlide(movex);
		},
		// On touch end event
		touchEnd: function (e) {
			e.stopPropagation();
			if (!vars.slide) {
				return false;
			}

			var touch = (e.changedTouches || e.touches || e.originalEvent.touches)[0];
			if (!private_function.isFirstTouchEvent(touch)) {
				return false;
			}

			vars.touch.first = null;
			var movex = vars.touch.x - touch.clientX,
			    movey = vars.touch.y - touch.clientY,
					index = null,
					duration = new Date() - vars.time;

            if ((private_function.isLayoutSwipe() || (duration < defaults.duration && Math.abs(movey) < defaults.touch.blockYTolerance)) && vars.block === true) {
                vars.touch.x = null;
                vars.touch.y = null;
				if (Math.abs(movex) > private_function.calculatePercentageToPixel(defaults.touch.tolerance) || (duration < defaults.duration && Math.abs(movey) < defaults.touch.blockYTolerance)) {
					index = private_function.getNextIndex(movex);
				} else {
					index = defaults.active;
				}

				private_function.slide(index);
			}
		},

        initTouchEvents: function () {
            defaults.target.addEventListener('touchstart', private_function.touchStart, {passive: true});
            defaults.target.addEventListener('touchend', private_function.touchEnd, {passive: true});
            defaults.target.addEventListener('touchmove', private_function.touchMove, {passive: true});
        },

        initAnimationEndEvent: function () {
            var events = ['transitionend', 'oTransitionEnd', 'webkitTransitionEnd'];
            for (var i = 0; i < events.length; i++) {
                vars.handlers.columns.addEventListener(events[i], private_function.unBlockColumns);
            }
        },

        initSlideshow: function(){
		    if(defaults.auto > 0 && vars.interval === null){
                vars.interval = setInterval(function(){
                    var index = private_function.getNextIndex(1);
                    if(!defaults.continuous){
                        index = private_function.checkIndex(index);
                        if(index === vars.countColumns-1){
                            private_function.offSlideshow();
                        }
                    }
                    public_functions.slide(index);
                }, defaults.auto);
            }
        },

        offSlideshow: function(){
		    if(vars.interval !== null){
		        clearInterval(vars.interval);
		        vars.interval = null;
            }
        },

        callEvent: function(index, event){
            if(typeof event === 'function'){
                if(defaults.continuous){
                    if(index < 0){
                        index = vars.countColumns-1;
                    }
                    else if(index >= vars.countColumns){
                        index = 0;
                    }
                }

                var element = defaults.target.querySelector(defaults.classes.column+'[data-index="'+index+'"]');
                event(index, element);
            }
        },

		isLayoutSwipe: function () {
			var tolerancePixels = private_function.calculatePercentageToPixel(defaults.touch.startTolerance);
			if (vars.touch.x < tolerancePixels || vars.touch.x > window.innerWidth - tolerancePixels) {
				return true;
			}
			return false;
		},

		isFirstTouchEvent: function (touch) {
			if (vars.touch.first !== null) {
				if (vars.touch.first.identifier === touch.identifier) {
					return true;
				}
			}
			return false;
		},

		blockColumns: function () {
			if (!vars.block) {
				vars.block = true;
				var activeElement = defaults.target.querySelector(defaults.classes.column + '.active');
				vars.blockActiveElement = activeElement;
				if(activeElement){
                    activeElement.classList.remove('active');
                }
			}
		},

		unBlockColumns: function () {
			if (vars.block) {
				vars.block = false;
				var count = vars.countColumns-1,
						moved = defaults.target.querySelector(defaults.classes.column+'.moved');

				vars.handlers.columns.classList.remove('animation');
				if(defaults.active < 0){
					defaults.active = count;
					vars.handlers.columns.style.marginLeft = '-'+(count*100)+'%';
				}
				else{
					if(defaults.active > count){
						defaults.active = 0;
						vars.handlers.columns.style.marginLeft = '-0%';
					}
				}
				if(moved){
					moved.style.transform = 'none';
					moved.classList.remove('moved');
				}

				vars.slide = true;
				var elementToActive = defaults.target.querySelector(defaults.classes.column + ':nth-child(' + (defaults.active + 1) + ')');
				elementToActive.classList.add('active');
				private_function.initSlideshow();
			}
		},

		getNextIndex: function(movex){
			var direction = 'left',
                index = defaults.active;

			if (movex > 0) {
				direction = 'right';
			}
			if (direction === 'left') {
				index = defaults.active - 1;
			} else {
				index = defaults.active + 1;
			}

			return index;
		},

		checkIndex: function(index){
			if(index < 0){
				return 0;
			}
			else if(index > vars.countColumns-1){
				return vars.countColumns-1;
			}
			else{
				return index;
			}
		},

        calculatePercentageToPixel: function (percentage) {
            return (percentage / 100) * window.innerWidth;
        },

        setFirstLastPosition: function(movex){
            var activeIndex = null;
            if(defaults.target.querySelector(defaults.classes.column+'.active')){
                activeIndex = defaults.target.querySelector(defaults.classes.column+'.active').getAttribute('data-index');
            }
            else{
                activeIndex = vars.blockActiveElement.getAttribute('data-index');
            }

            var count = defaults.target.querySelectorAll(defaults.classes.column).length-1,
                moved = defaults.target.querySelector(defaults.classes.column+'.moved');

            if(moved){
                moved.style.transform = 'none';
                moved.classList.remove('moved');
            }
            if(activeIndex !== count.toString() && movex < 0){
                var element = defaults.target.querySelector(defaults.classes.column+'[data-index="'+count+'"]');
                element.style.transform = 'translate(-'+((count+1)*100)+'%)';
                element.classList.add('moved');
            }
            else{
                if(activeIndex !== '0' && movex > 0){
                    var element = defaults.target.querySelector(defaults.classes.column+'[data-index="0"]');
                    element.classList.add('moved');
                    element.style.transform = 'translate('+((count+1)*100)+'%)';
                }
            }
        },

		slide: function (index) {
			if (vars.slide) {
				vars.slide = false;

				if(!defaults.continuous){
					index = private_function.checkIndex(index);
				}

				private_function.callEvent(index, defaults.events.onInitColumn);

				private_function.blockColumns();
				vars.handlers.columns.classList.add('animation');
				vars.handlers.columns.style.marginLeft = (index * -100) + '%';
				defaults.active = index;
			}
		},

        moveSlide: function (movex) {
            if (Math.abs(movex) > defaults.touch.blockTolerance && private_function.isLayoutSwipe()) {
                var index = private_function.getNextIndex(movex);
                if(!defaults.continuous){
                    index = private_function.checkIndex(index);
                }
                private_function.blockColumns();
                private_function.callEvent(index, defaults.events.onInitColumn);
                var fit = defaults.touch.blockTolerance;
                if(movex < 0){
                    fit = -defaults.touch.blockTolerance;
                }
                vars.handlers.columns.style.marginLeft = 'calc(' + (defaults.active * -100) + '% - ' + (movex-fit) + 'px)';
            }
            else{
                vars.handlers.columns.style.marginLeft = (defaults.active * -100) + '%';
            }
        },
        setContainer: function(){
            vars.handlers.columns = defaults.target.querySelector(defaults.classes.columns);
            vars.countColumns = defaults.target.querySelectorAll(defaults.classes.column).length;

            vars.handlers.columns.style.width = (vars.countColumns*100)+'%';

            var columns = defaults.target.querySelectorAll(defaults.classes.column);

            var width = (100 / vars.countColumns)+'%';
            for(var i = 0; i < columns.length; i++){
                columns[i].style.width = width;
            }

            private_function.slide(defaults.active);
            private_function.unBlockColumns();
        }
    },
	public_functions = {
		slide: function (index) {
			if (index !== defaults.active) {
			    if(defaults.continuous){
                    private_function.setFirstLastPosition(index-defaults.active);
                }

                private_function.slide(index);
			}
		},
	},
	init = function () {
			defaults = Object.assign(defaults, options);

			private_function.setContainer();
			private_function.initAnimationEndEvent();
			private_function.initTouchEvents();
			private_function.initSlideshow();
	};

	init();

    return public_functions;
};